import os
import sys
import csv
from pypdf import PdfMerger


SESSION_DATA = "data/uai_sessions.csv"
PRESENTATION_DATA = "data/uai_presentations.csv"

def write_preamble(out_file):
    # write the preamble
    out_file.write("\\documentclass[11pt]{beamer}\n")
    out_file.write("\\usepackage[utf8]{inputenc}\n")
    out_file.write("\\usepackage{graphicx}\n")
    out_file.write("\\usepackage{tikz,pgf}")
    out_file.write("\\usepackage{tikzsymbols}")
    out_file.write("\\usepackage{pdfpages}")
    out_file.write("\\setbeamertemplate{navigation symbols}{}")
    out_file.write("\\definecolor{uaicolor}{RGB}{62,53,123}\n\n\n")
    out_file.write("\\begin{document}\n\n\n")

def write_intro(out_file, session, start, end, date):
    # write the intro slide
    out_file.write("\\usebackgroundtemplate{\includegraphics[width=\paperwidth]{images/connection-sponsors}}\n")
    out_file.write("\\begin{frame}\n")
    out_file.write("\\begin{center}\n")
    out_file.write("\\color{uaicolor}")
    out_file.write("\\LARGE \\bf {}\\\\ [1em]\n".format(session))
    out_file.write("{}-{}, {}\n".format(start, end, date))
    out_file.write("\\end{center}\n")
    out_file.write("\\end{frame}\n")
    
def write_connector(out_file, paper_id, session, paper_title, authors, speaker):
    # write connector slide
    out_file.write("\\usebackgroundtemplate{\includegraphics[width=\paperwidth]{images/connection}}\n")
    out_file.write("\\begin{frame}\n")
    out_file.write("\\begin{center}\n")
    out_file.write("\\color{uaicolor} \\bf\n")
    out_file.write("Paper ID: {} \\qquad\\qquad {} \\\\[1.5em]\n".format(paper_id, session))
    out_file.write("{{\\LARGE {}}} \\\\[1.5em]\n".format(paper_title))
    out_file.write("{} \\\\[1.5em]\n".format(authors))
    out_file.write("{{\\LARGE Speaker: {}}}\n".format(speaker))
    out_file.write("\\end{center}\n")
    out_file.write("\\end{frame}\n")

def write_connector_discussant(out_file, paper_id, session, paper_title, authors, discussant):
    # write connector slide
    out_file.write("\\usebackgroundtemplate{\includegraphics[width=\paperwidth]{images/connection}}\n")
    out_file.write("\\begin{frame}\n")
    out_file.write("\\begin{center}\n")
    out_file.write("\\color{uaicolor} \\bf\n")
    out_file.write("Paper ID: {} \\qquad\\qquad {} \\\\[1.5em]\n".format(paper_id, session))
    out_file.write("{{\\LARGE {}}} \\\\[1.5em]\n".format(paper_title))
    out_file.write("{} \\\\[1.5em]\n".format(authors))
    out_file.write("{{\\LARGE Discussant: {}}}\n".format(discussant))
    out_file.write("\\end{center}\n")
    out_file.write("\\end{frame}\n")
    

def get_session_details(session_type, session_number):
    # get session details

    with open(SESSION_DATA, 'r') as in_file:

        in_file = csv.reader(in_file)
        header = next(in_file)
        type_index = header.index("Session Type")
        number_index = header.index("Session Number")
        name_index = header.index("Session Name")
        start_index = header.index("Start Time")
        end_index = header.index("End Time")
        date_index = header.index("Date")

        print(session_type, session_number)

        for row in in_file:
            if row[type_index] == session_type and row[number_index] == session_number:
                return row[name_index], row[start_index], row[end_index], row[date_index]
       

def main():

    session_name = sys.argv[1]

    session_type, session_number = session_name[:-2], session_name[-1]

    print(session_type, session_number)
    title, start, end, date = get_session_details(session_type, session_number)


    intro_file = open("intro.tex", 'w')
    write_preamble(intro_file)
    write_intro(intro_file, session_name, start, end, date)
    intro_file.write("\\end{document}\n")
    intro_file.close()
    os.system("pdflatex intro.tex")
    pdfs = ["intro.pdf"]
    
    with open(PRESENTATION_DATA, 'r') as in_file:
        
        in_file = csv.reader(in_file)
        header = next(in_file)
        id_index = header.index("Paper ID")
        title_index = header.index("Paper title")
        author_index = header.index("Paper authors")
        speaker_index = header.index("Speaker")
        session_index = header.index("Session name")

        for row in in_file:

            if row[session_index] != session_name:
                continue

            paper_id = row[id_index]
            title = row[title_index]
            authors = row[author_index]
            speaker = row[speaker_index]
            pdfs.append("spotlight{}.pdf".format(paper_id))
            pdfs.append("spotlights/{}.pdf".format(paper_id))

            tex_file = open("spotlight{}.tex".format(paper_id), 'w')
            write_preamble(tex_file)
            write_connector(tex_file, paper_id, session_name, title, authors, speaker)
            tex_file.write("\\end{document}\n")
            tex_file.close()
            os.system("lualatex spotlight{}.tex".format(paper_id))

    merger = PdfMerger()

    missing_pdfs = []

    for pdf in pdfs:
        if not os.path.exists(pdf):
            missing_pdfs.append(pdf)
        else:
            merger.append(pdf)

    merger.write("complete_spotlight_session_{}.pdf".format(session_number))
    merger.close()
    
    os.system("rm intro*")
    os.system("rm spotlight*")

    print("MISSING PDFS", missing_pdfs)



if __name__ == "__main__":

    main()

